//	2. Escribir un programa en C que intercambie dos array usando punteros.
#include <stdio.h>
#define tam 5

int main()
{ 
	int array1[tam];
	int array2[tam];
	int a,b,temp;
	int num;
	printf("\nIngrese los datos del array 1\n");
	for(int i=0;i<tam;i++)
	{
		scanf("%d", &num);			
		array1[i]=num;
		}
	printf("\nIngrese los datos del array 2\n");
	for(int j=0;j<tam;j++)
	{
		scanf("%d", &num);
		array2[j]=num;
		}
		//intercambio de variables
		for(int i=0;i<tam;i++)
		{
		int *pA,*pB,aux;
		pA=&array1[i];
		pB=&array2[i];
		aux=*pA;
		pA=pB;
		pB=&aux;
		array1[i]=*pA;
		array2[i]=*pB;	
			}
		//Imprimir los arrays
		for(int i=0;i<tam;i++)
		{
			printf("\nLos nuevos datos del array 1 son: %d\n", array1[i]);
			}
		for(int i=0;i<tam;i++)
		{
			printf("\nLos nuevos datos del array 2 son: %d\n", array2[i]);
			}
return 0;
}
